package bitbucket_pack;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Bucket {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		WebDriver driver;
		
		System.setProperty("webdriver.gecko.driver", "D:\\Eclipse\\geckodriver-v0.26.0-win64//geckodriver.exe");
		
		driver = new FirefoxDriver();
		
		driver.get("http://demo.guru99.com/test/guru99home/");
		
		driver.manage().window().maximize();
		
		driver.switchTo().frame("a077aa5e");
		
		System.out.println("We are switch to the iframe");
		
		driver.findElement(By.xpath("/html/body/a/img")).click();
		
		System.out.println("We are done with the test");

	}

}
